# ASL's AWS ChatOps Demo pack

This pack is a demonstration of how StackStorm and it's ChatOps can be used to monitor AWS EC2 resources. It provides a workflow that will:

- check what EC2 instances are running in specified region(s)
- check if those instances are in an exempt Excel spreadsheet
- report which non-exempt instances are running, and ask via Slack if they should be stopped
- if given authorisation, it will stop EC2 instances. If no authorisation is given, it will raise a ServiceNow incident.

It also provides aliases to manage the list of exempt instances in the Excel spreadsheet, as well as aliases for inquiring, starting and stopping instances in an AWS region.

## Configuration File

No configuraton file is required

## Exempt Instances

The demonstration uses an Excel spreadsheet to specify which EC2 instances are exempt. An example spreadsheet ec2_exempt.xlsx is provided. The demonstration hardcodes the location of that spreadsheet to /opt/stackstorm/ec2_exempt.xlsx and it must be manually installed.

The spreadsheet is organised with a single sheet per region, and will create a new sheet if an exempt instance is added for a new region.

Explicit actions for managing the exempt instances have been written, to keep the aliases simple to use.

## Pre-requisites
Dependant packs are installed and configured:

- aws_boto3
- excel
- servicenow

ChatOps is configured with Slack as the ChatOps Provider. The demonstration hardcodes the Slack channel to aws-chatops-demo.

AWS credentials are set up with a DEFAULT profile.

Exempt instances spreadsheet is deployed, either using the example or just a blank spreadsheet.

ServiceNow pack needs to be configured. The rules in pack are currently hardcoded to use the user testuser, either edit the rules or create this user.

## Actions

The pack comes with the following actions:

- demo_aws_ec2_running_regions
- demo_aws_ec2_running_region
- add_exempt
- describe_exempt
- list_exempt
- remove_exempt

#### demo_aws_ec2_running_regions

Outer workflow that cycles round an array of regions to check which regions have non-exempt EC2 instances running.

Parameters:

- regions: Array of regions to check
- sn_user: ServiceNow user to use to create incidents with

#### demo_aws_ec2_running_region

Main workflow which checks what instances are running in a specified region. It checks against the exempt spreadsheet and reports to Slack if any non-exempt instances are running. The user can choose to stop or leave instances running. If they choose to stop, the workflow will stop instances. If they choose to leave instances running, the workflow will raise an incident in ServiceNow.

Parameters:

- region: Region to check
- sn_user: ServiceNow user to use to create incidents with

#### add_exempt

Adds an instance to exempt Excel file (with instanceid and comments). 

Parameters:

- region: Region instance resides in
- instanceid: AWS Instance Id to add
- description: A description of the instance, returned by the describe_exempt action 

#### describe_exempt

Describes an instance in exempt Excel file (reports the comments attached to that instances).

Parameters:

- region: Region instance resides in
- instanceid: AWS Instance Id to describe

#### list_exempt

Lists instances in exempt Excel file

Parameters:

- region: Region to inquire on

#### remove_exempt

Removes an instance from the exempt Excel file

Parameters:

- region: Region instance resides in
- instanceid: AWS Instance Id to remove

## Aliases

The pack comes with the following aliases:

- !ec2 add exempt {{instanceid}} {{description}} [region] - Add exempt EC2 instance
- !ec2 describe exempt {{instanceid}} [region] - Describe exempt EC2 instance
- !ec2 get [region] - Retrieves EC2 instances in a single AWS region (defaults to eu-west-1)
- !ec2 list exempt [region] - List exempt EC2 instances in a single AWS region
- !ec2 remove exempt {{instanceid}} [region] - Remove exempt EC2 instances in a single AWS region
- !ec2 start {"InstanceIds": ["<instanceId>"]} [region] - Starts one or more EC2 instances in a region (defaults to eu-west-1)
- !ec2 stop {"InstanceIds": ["<instanceId>"]} [region] - Stops one or more EC2 instances in a region (defaults to eu-west-1)
- !authorise_ec2_stop {{ id }} - Respond positively to an ec2 stop inquiry
- !unauthorise_ec2_stop {{ id }} - Respond negatively to an EC2 stop inquiry

## Rules

The pack comes with the following rules:

- demo_ask_stop - rule for handling the core.ask in the workflow, and reporting what aliases to use to determine whether to stop or leave instances running.
- demo_aws_monitor_weekday_eu_running - runs the workflow for eu-west-1, eu-west-2 and eu-west-3 regions out of hours on weekdays
- demo_aws_monitor_weekend_eu_running - runs the workflow for eu-west-1, eu-west-2 and eu-west-3 regions on weekends

## Further assistance

If you would like any further information or guidance on the demo, or help in achieving your automation goals,  then do please get in contact with us at Ammeon Solutions via [Ammeon Solutions: Contact Us](https://www.ammeonsolutions.com/contact) or the author at amanda.mcguinness@ammeonsolutions.com. See [our StackStorm page](https://www.ammeonsolutions.com/stackstorm) for further information on how Ammeon Solutions can help you achieve your automation goals using StackStorm. We recommend StackStorm to customers for a range of solutions including; streamlining fault detection and recovery, repetitive task automation and providing users with a single view across multiple domains and tools. 

[Ammeon Solutions](https://www.ammeonsolutions.com) have a history steeped in operating critical infrastructure. Our mission is to automate all aspects of IT Infrastructure and Operations through the deployment of open source tooling coupled with enterprise-grade support. 
