"""
Copyright 2020 Ammeon Solutions

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from st2common.runners.base_action import Action

from st2client.client import Client
from st2client.models import LiveAction

import time
import st2client.commands.action


class ExemptAction(Action):

    PENDING_STATUSES = [
        st2client.commands.action.LIVEACTION_STATUS_REQUESTED,
        st2client.commands.action.LIVEACTION_STATUS_SCHEDULED,
        st2client.commands.action.LIVEACTION_STATUS_RUNNING,
        st2client.commands.action.LIVEACTION_STATUS_CANCELING
    ]

    @staticmethod
    def action_execute(server, action, params):
        client = Client(base_url="https://{}/".format(server))
        execution = client.liveactions.create(LiveAction(action=action, parameters=params))

        # wait for the execution to finish
        while execution.status in ExemptAction.PENDING_STATUSES:
            time.sleep(1)
            execution = client.liveactions.get_by_id(execution.id)

        if execution.status != st2client.commands.action.LIVEACTION_STATUS_SUCCEEDED:
            failure_reason = "Failed to examine excel for {} - see execution {}".format(region,execution.id)
            raise Exception(failure_reason)
        return execution
  
    def get_base_params(self, region):
        base_excel_params = {
                    "sheet":"exempt_instances_{}".format(region),
                    "excel_file": "/opt/stackstorm/ec2_exempt.xlsx"}
        return base_excel_params

